package co.edu.uniandes.testtool.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ArchivosUtil {
	
	public static void cambiarTexto(String archivo, String palabraClave, String cadenaDestino) {
		BufferedReader fileReader = null;
		BufferedWriter fileWriter = null;
		List <String> lineas = new ArrayList<>();
		try {
			fileReader = new BufferedReader(new FileReader(archivo));
			String line;
	        while ((line = fileReader.readLine()) != null) {
	        	if (line.contains(palabraClave)) {
	        		line = cadenaDestino;
	        	}
        		lineas.add(line);
	        }
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				fileReader.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		try {
			fileWriter = new BufferedWriter(new FileWriter(archivo));
			for(String l : lineas) {
				fileWriter.append(l);
				if (!l.equals("") && !l.contains("\n")) {
					fileWriter.newLine();
				}
			}
			fileWriter.flush();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				fileWriter.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public static void excluir(String archivo, String tagInicio, String tagFinal) {
		BufferedReader fileReader = null;
		BufferedWriter fileWriter = null;
		List <String> lineas = new ArrayList<>();
		try {
			fileReader = new BufferedReader(new FileReader(archivo));
			String line;
			boolean band = false;
	        while ((line = fileReader.readLine()) != null) {
	        	if (line.contains(tagInicio) && !band) {
	        		if (tagInicio.split("class").length == 2) {
	        			lineas.add(line);
	        		}
	        		band = true;
	        	}else if (line.contains(tagFinal) && band) {
	        		band = false;
	        	}
	        	if (!band) {
	        		lineas.add(line);
	        	}
	        }
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				fileReader.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		try {
			fileWriter = new BufferedWriter(new FileWriter(archivo));
			for(String l : lineas) {
				fileWriter.append(l);
				if (!l.equals("") && !l.contains("\n")) {
					fileWriter.newLine();
				}
			}
			fileWriter.flush();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				fileWriter.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static List<String> ocurrencias(File archivoSeleccionado, String ocurrencia) {
		BufferedReader fileReader = null;
		List <String> lineas = new ArrayList<>();
		try {
			fileReader = new BufferedReader(new FileReader(archivoSeleccionado));
			String line;
	        while ((line = fileReader.readLine()) != null) {
	        	if (line.contains(ocurrencia) && line.endsWith(";")) {
	        		lineas.add(line);
	        	}
	        }
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				fileReader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return lineas;
	}
	
	public static void cambiarTextoMutante(String archivo, String palabraClave, String cadenaDestino) {
		BufferedReader fileReader = null;
		BufferedWriter fileWriter = null;
		List <String> lineas = new ArrayList<>();
		try {
			fileReader = new BufferedReader(new FileReader(archivo));
			String line;
	        while ((line = fileReader.readLine()) != null) {
	        	if (line.contains(palabraClave)) {
	        		line = cadenaDestino;
	        	}
        		lineas.add(line);
	        }
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				fileReader.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		try {
			fileWriter = new BufferedWriter(new FileWriter(archivo));
			for(String l : lineas) {
				fileWriter.append(l);
				fileWriter.newLine();
			}
			fileWriter.flush();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				fileWriter.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static List<String> ocurrenciasCORSimples(File archivoSeleccionado) {
		BufferedReader fileReader = null;
		List <String> lineas = new ArrayList<>();
		try {
			fileReader = new BufferedReader(new FileReader(archivoSeleccionado));
			String line;
	        while ((line = fileReader.readLine()) != null) {
	        	String[] splitAnd = line.split("&&");
	        	String[] splitOr = line.split("\\|\\|");
	        	if (splitAnd.length == 2 || 
	        			splitOr.length == 2) {
	        		lineas.add(line);	        		
	        	}
	        }
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				fileReader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return lineas;
	}
	
	public static List<String> ocurrenciasAORIncrement(File archivoSeleccionado) {
		BufferedReader fileReader = null;
		List <String> lineas = new ArrayList<>();
		try {
			fileReader = new BufferedReader(new FileReader(archivoSeleccionado));
			String line;
	        while ((line = fileReader.readLine()) != null) {
	        	if (line.contains("++")) {
	        		lineas.add(line);	        		
	        	}
	        }
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				fileReader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return lineas;
	}
}