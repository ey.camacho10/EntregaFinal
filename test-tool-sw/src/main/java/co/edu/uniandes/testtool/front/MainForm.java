package co.edu.uniandes.testtool.front;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.Set;

import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import co.edu.uniandes.testtool.logica.MutanteService;
import co.edu.uniandes.testtool.logica.WebTestService;
import co.edu.uniandes.testtool.util.ArchivosUtil;
import co.edu.uniandes.testtool.util.Constantes;
import co.edu.uniandes.testtool.util.DistribucionEjecutada;

public class MainForm extends JFrame implements ActionListener {
    
	private JPanel contentPane;
	private JTextField textFieldNroEventos;
	private JList<String> listComandos;
	private JList<String> listComandosUsar;
	private DefaultListModel<String> modelComandos;
	private DefaultListModel<String> modelComandosUsar;
	private JTextField textFieldRutaApk;
	private JTextField textFieldPaquete;
	private Runtime rt = Runtime.getRuntime();
	private JTextField txtRutaADB;
	private Boolean primeraVez = true;
	private JTextField textFieldDistribucion;
	private Map<String, DistribucionEjecutada> mapaDistribucion;
	private JTextArea textAreaConsola;
	private ButtonGroup groupApps;
	private JTextField txtHttp;
	private JTextField textFieldDitribucion2;
	private DefaultListModel<String> modelEspecies;
	private DefaultListModel<String> modelEspeciesUsar;
	private JTextField textFieldSemilla;
	private JTextField txtNumBrowser;
	private JTextField txtC;
	private JTextField textField_3;
	private DefaultListModel<String> modelMutantes;
	private DefaultListModel<String> modelMutantesUsar;
	public static String DIRECTORIO_GREMLIN = "";
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainForm frame = new MainForm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainForm() {
		Properties prop = new Properties();
		String propFileName = "configuracion.properties";
		
		InputStream inputStream = null;
		try {
			inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
			prop.load(inputStream);
			DIRECTORIO_GREMLIN = prop.getProperty("gremlin.ruta");
			WebTestService.DIRECTORIO_GREMLIN = DIRECTORIO_GREMLIN;
		} catch (Exception e) {
			System.out.println("Exception: " + e);
		} finally {
			try {
				if (inputStream != null) {
					inputStream.close();
				}
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		setTitle("Automatic Testing Tool ");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 819, 842);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		modelComandos = new DefaultListModel<String>();
		String[] values = Constantes.COMANDOS.keySet().toArray(new String[] {});
		for (String v : values) {
			modelComandos.addElement(v);
			
		}
		modelComandosUsar = new DefaultListModel<String>();
		modelEspecies = new DefaultListModel<String>();
		modelEspeciesUsar = new DefaultListModel<String>();
		modelMutantes = new DefaultListModel<String>();
		modelMutantesUsar = new DefaultListModel<String>();
		
		String[] valuesEstrategias = Constantes.ESPECIES.keySet().toArray(new String[] {});
		Arrays.sort(valuesEstrategias);
		for (String v : valuesEstrategias) {
			modelEspecies.addElement(v);
		}
		
		String[] valuesMutantes = Constantes.MUTANTES.keySet().toArray(new String[] {});
		Arrays.sort(valuesMutantes);
		for (String v : valuesMutantes) {
			modelMutantes.addElement(v);
		}
		
		JPanel panel_3 = new JPanel();
		panel_3.setLayout(null);
		panel_3.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Tipos de aplicaci\u00F3n", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_3.setBounds(37, 69, 735, 61);
		contentPane.add(panel_3);
		
		JLabel lblculTipoDe = new JLabel("\u00BFCu\u00E1l tipo de aplicaci\u00F3n?");
		lblculTipoDe.setBounds(24, 28, 154, 16);
		panel_3.add(lblculTipoDe);
		
		JRadioButton rdbtnWeb = new JRadioButton("Web");
		rdbtnWeb.setSelected(true);
		rdbtnWeb.setBounds(246, 24, 73, 25);
		panel_3.add(rdbtnWeb);
		
		JRadioButton rdbtnAndroid = new JRadioButton("Android");
		rdbtnAndroid.setBounds(389, 24, 127, 25);
		panel_3.add(rdbtnAndroid);
		
		JLabel lblTestAutomaticTool = new JLabel("Automatic Testing Tool SW");
		lblTestAutomaticTool.setFont(new Font("Times New Roman", Font.BOLD, 24));
		lblTestAutomaticTool.setBounds(239, 13, 290, 51);
		contentPane.add(lblTestAutomaticTool);
		
		JRadioButton rdbtnMutante = new JRadioButton("Mutante");
		rdbtnMutante.setBounds(531, 25, 127, 25);
		panel_3.add(rdbtnMutante);
		
		groupApps = new ButtonGroup();
		groupApps.add(rdbtnWeb);
		groupApps.add(rdbtnAndroid);
		groupApps.add(rdbtnMutante);
				
		JPanel panel_4 = new JPanel();
		panel_4.setBounds(12, 128, 762, 602);
		contentPane.add(panel_4);
		panel_4.setVisible(false);
		panel_4.setLayout(null);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBounds(12, 13, 735, 63);
		panel_2.setBorder(new TitledBorder(null, "Configuracion ADB", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		
		JLabel lblRutaAdb = new JLabel("Ruta ADB");
		lblRutaAdb.setBounds(13, 28, 348, 16);
		
		txtRutaADB = new JTextField();
		txtRutaADB.setBounds(146, 25, 550, 22);
		txtRutaADB.setText("C:/Users/Edgar-pc/AppData/Local/Android/sdk/platform-tools/");
		txtRutaADB.setColumns(10);
		panel_4.add(panel_2);
		panel_2.setLayout(null);
		panel_2.add(lblRutaAdb);
		panel_2.add(txtRutaADB);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(12, 89, 738, 114);
		panel_4.add(panel_1);
		panel_1.setBorder(new TitledBorder(null, "Instalacion APK", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.setLayout(null);
		
		JLabel label_1 = new JLabel("Ruta APK a instalar");
		label_1.setBounds(30, 25, 235, 16);
		panel_1.add(label_1);
		
		textFieldRutaApk = new JTextField();
		textFieldRutaApk.setText("C:/APK/\"Omni Notes_v5.3.2_apkpure.com.apk\"");
		textFieldRutaApk.setColumns(10);
		textFieldRutaApk.setBounds(40, 54, 665, 22);
		panel_1.add(textFieldRutaApk);
		
		JLabel lblPaqueteMainactivity = new JLabel("Paquete MainActivity");
		lblPaqueteMainactivity.setBounds(30, 85, 235, 16);
		panel_1.add(lblPaqueteMainactivity);
		
		textFieldPaquete = new JTextField();
		textFieldPaquete.setText("it.feio.android.omninotes");
		textFieldPaquete.setColumns(10);
		textFieldPaquete.setBounds(209, 82, 496, 22);
		panel_1.add(textFieldPaquete);
		
		JPanel panel = new JPanel();
		panel.setBounds(12, 216, 733, 276);
		panel_4.add(panel);
		panel.setBorder(new TitledBorder(null, "Comandos", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("N\u00FAmero de Eventos");
		lblNewLabel.setBounds(35, 30, 159, 16);
		panel.add(lblNewLabel);
		
		textFieldNroEventos = new JTextField();
		textFieldNroEventos.setBounds(163, 27, 135, 22);
		panel.add(textFieldNroEventos);
		textFieldNroEventos.setColumns(10);
		
		JLabel lblComandosAEjecutar = new JLabel("Seleccione Comandos");
		lblComandosAEjecutar.setBounds(35, 73, 159, 16);
		panel.add(lblComandosAEjecutar);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(59, 102, 225, 91);
		panel.add(scrollPane);
		
		listComandos = new JList<>();
		scrollPane.setViewportView(listComandos);
		listComandos.setModel(modelComandos);
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(458, 102, 225, 91);
		panel.add(scrollPane_1);
		
		listComandosUsar = new JList<>();
		scrollPane_1.setViewportView(listComandosUsar);
		listComandosUsar.setVisibleRowCount(3);
		listComandosUsar.setModel(modelComandosUsar);
		
		JLabel label = new JLabel("Comandos a usar");
		label.setBounds(440, 73, 159, 16);
		panel.add(label);
		
		JButton btnPasar = new JButton(">");
		btnPasar.addActionListener(this);
		btnPasar.setBounds(347, 86, 56, 25);
		panel.add(btnPasar);
		
		JButton btnPasarTodo = new JButton(">>");
		btnPasarTodo.addActionListener(this);
		btnPasarTodo.setBounds(347, 113, 56, 25);
		panel.add(btnPasarTodo);
		
		JButton btnQuitarTodo = new JButton("<<");
		btnQuitarTodo.addActionListener(this);
		btnQuitarTodo.setBounds(347, 140, 56, 25);
		panel.add(btnQuitarTodo);
		
		JButton btnQuitar = new JButton("<");
		btnQuitar.addActionListener(this);
		btnQuitar.setBounds(347, 168, 56, 25);
		panel.add(btnQuitar);
		
		JButton btnEjecutar = new JButton("Ejecutar");
		btnEjecutar.addActionListener(this);
		btnEjecutar.setBounds(318, 241, 97, 25);
		panel.add(btnEjecutar);
		
		JLabel lblDistribucin = new JLabel("Distribuci\u00F3n");
		lblDistribucin.setBounds(35, 209, 235, 16);
		panel.add(lblDistribucin);
		
		textFieldDistribucion = new JTextField();
		textFieldDistribucion.setToolTipText("Forma: 0.4,0.4,0.2");
		textFieldDistribucion.setColumns(10);
		textFieldDistribucion.setBounds(162, 206, 536, 22);
		panel.add(textFieldDistribucion);
		
		JLabel lblForma = new JLabel("Forma: 0.4,0.4,0.2");
		lblForma.setBounds(44, 225, 235, 16);
		panel.add(lblForma);
		
		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(14, 546, 733, 99);
		panel_4.add(scrollPane_2);
		
		textAreaConsola = new JTextArea();
		scrollPane_2.setViewportView(textAreaConsola);
		
		JPanel panel_7 = new JPanel();
		panel_7.setVisible(false);
		panel_7.setLayout(null);
		panel_7.setBounds(31, 128, 762, 412);
		contentPane.add(panel_7);
		
		JPanel panel_8 = new JPanel();
		panel_8.setLayout(null);
		panel_8.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Configuraci\u00F3n", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_8.setBounds(12, 13, 733, 301);
		panel_7.add(panel_8);
		
		JLabel lblRuta = new JLabel("Ruta");
		lblRuta.setBounds(35, 30, 37, 16);
		panel_8.add(lblRuta);
		
		txtC = new JTextField();
		txtC.setText("C:/");
		txtC.setColumns(10);
		txtC.setBounds(73, 27, 625, 22);
		panel_8.add(txtC);
		
		JLabel lblSeleccioneMutantes = new JLabel("Seleccione Tipos de Mutantes");
		lblSeleccioneMutantes.setBounds(35, 73, 159, 16);
		panel_8.add(lblSeleccioneMutantes);
		
		JScrollPane scrollPane_6 = new JScrollPane();
		scrollPane_6.setBounds(59, 102, 225, 91);
		panel_8.add(scrollPane_6);
		
		JList<String> list = new JList<String>();
		scrollPane_6.setViewportView(list);
		list.setModel(modelMutantes);
		
		JScrollPane scrollPane_7 = new JScrollPane();
		scrollPane_7.setBounds(458, 102, 225, 91);
		panel_8.add(scrollPane_7);
		
		JList<String> list_1 = new JList<String>();
		scrollPane_7.setViewportView(list_1);
		list_1.setModel(modelMutantesUsar);
		
		JLabel lblTiposDeMutantes = new JLabel("Tipos de Mutantes a usar");
		lblTiposDeMutantes.setBounds(440, 73, 159, 16);
		panel_8.add(lblTiposDeMutantes);
		
		JButton button_4 = new JButton(">");
		button_4.setBounds(347, 86, 56, 25);
		panel_8.add(button_4);
		
		JButton button_5 = new JButton(">>");
		button_5.setBounds(347, 113, 56, 25);
		panel_8.add(button_5);
		
		JButton button_6 = new JButton("<<");
		button_6.setBounds(347, 140, 56, 25);
		panel_8.add(button_6);
		
		JButton button_7 = new JButton("<");
		button_7.setBounds(347, 168, 56, 25);
		panel_8.add(button_7);
		
		JButton btnCrearMutantes = new JButton("Crear Mutantes");
		btnCrearMutantes.setBounds(214, 254, 148, 25);
		panel_8.add(btnCrearMutantes);
		
		JLabel lblNmeroDeMutantes = new JLabel("Número de mutantes");
		lblNmeroDeMutantes.setBounds(35, 219, 140, 16);
		panel_8.add(lblNmeroDeMutantes);
		
		textField_3 = new JTextField();
		textField_3.setText("1");
		textField_3.setColumns(10);
		textField_3.setBounds(185, 216, 61, 22);
		panel_8.add(textField_3);
		
		JButton btnReporteMutantes = new JButton("Ver Reporte");
		btnReporteMutantes.setBounds(427, 254, 124, 25);
		panel_8.add(btnReporteMutantes);
		
		JPanel panel_5 = new JPanel();
		panel_5.setBounds(0, 128, 762, 543);
		contentPane.add(panel_5);
		panel_5.setLayout(null);
		
		JPanel panel_6 = new JPanel();
		panel_6.setLayout(null);
		panel_6.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Configuraci\u00F3n", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_6.setBounds(12, 13, 733, 386);
		panel_5.add(panel_6);
		
		JLabel lblUrl = new JLabel("URL");
		lblUrl.setBounds(35, 30, 37, 16);
		panel_6.add(lblUrl);
		
		txtHttp = new JTextField();
		txtHttp.setText("http://www.elempleo.com/co/");
		txtHttp.setColumns(10);
		txtHttp.setBounds(73, 27, 625, 22);
		panel_6.add(txtHttp);
		
		JLabel lblSeleccioneEstrategias = new JLabel("Seleccione Estrategias");
		lblSeleccioneEstrategias.setBounds(35, 73, 159, 16);
		panel_6.add(lblSeleccioneEstrategias);
		
		JScrollPane scrollPane_3 = new JScrollPane();
		scrollPane_3.setBounds(59, 102, 225, 91);
		panel_6.add(scrollPane_3);
		
		JList<String> listEspecies = new JList<String>();
		scrollPane_3.setViewportView(listEspecies);
		listEspecies.setModel(modelEspecies);
		
		JScrollPane scrollPane_4 = new JScrollPane();
		scrollPane_4.setBounds(458, 102, 225, 91);
		panel_6.add(scrollPane_4);
		
		JList<String> listEspeciesUsar = new JList<String>();
		listEspeciesUsar.setVisibleRowCount(3);
		scrollPane_4.setViewportView(listEspeciesUsar);
		listEspeciesUsar.setModel(modelEspeciesUsar);
		
		JLabel lblEstategiasAUsar = new JLabel("Estategias a usar");
		lblEstategiasAUsar.setBounds(440, 73, 159, 16);
		panel_6.add(lblEstategiasAUsar);
		
		JButton button = new JButton(">");
		button.setBounds(347, 86, 56, 25);
		panel_6.add(button);
		
		JButton button_1 = new JButton(">>");
		button_1.setBounds(347, 113, 56, 25);
		panel_6.add(button_1);
		
		JButton button_2 = new JButton("<<");
		button_2.setBounds(347, 140, 56, 25);
		panel_6.add(button_2);
		
		JButton button_3 = new JButton("<");
		button_3.setBounds(347, 168, 56, 25);
		panel_6.add(button_3);
		
		JButton btnEjecutar2 = new JButton("Ejecutar");
		btnEjecutar2.setBounds(244, 350, 97, 25);
		panel_6.add(btnEjecutar2);
		
		JLabel label_5 = new JLabel("Distribuci\u00F3n");
		label_5.setBounds(35, 230, 115, 16);
		panel_6.add(label_5);
		
		textFieldDitribucion2 = new JTextField();
		textFieldDitribucion2.setToolTipText("Forma: 0.4,0.4,0.2");
		textFieldDitribucion2.setColumns(10);
		textFieldDitribucion2.setBounds(162, 227, 241, 22);
		panel_6.add(textFieldDitribucion2);
		
		JLabel label_6 = new JLabel("Forma: 0.4,0.4,0.2");
		label_6.setBounds(44, 246, 235, 16);
		panel_6.add(label_6);
		
		JCheckBox chckbxHeadless = new JCheckBox("Headless");
		chckbxHeadless.setBounds(440, 226, 113, 25);
		panel_6.add(chckbxHeadless);
		
		JLabel lblSemilla = new JLabel("Semilla");
		lblSemilla.setBounds(35, 288, 115, 16);
		panel_6.add(lblSemilla);
		
		textFieldSemilla = new JTextField();
		textFieldSemilla.setToolTipText("Forma: 0.4,0.4,0.2");
		textFieldSemilla.setColumns(10);
		textFieldSemilla.setBounds(162, 286, 241, 22);
		panel_6.add(textFieldSemilla);
		
		JScrollPane scrollPane_5 = new JScrollPane();
		scrollPane_5.setBounds(12, 431, 733, 99);
		panel_5.add(scrollPane_5);
		
		JTextArea textAreaConsola2 = new JTextArea();
		scrollPane_5.setViewportView(textAreaConsola2);
		
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Object[] comandos = (Object[])listEspecies.getSelectedValues();
				Object[] comandosSel = modelEspeciesUsar.toArray();
				if (comandos != null && comandos.length > 0) {
					for (Object comando : comandos) {
						boolean esta = false;
						if (comandosSel != null && comandosSel.length > 0) {
							for (Object comandoSel : comandosSel) {
								if (comando.equals(comandoSel)) {
									esta = true;
									break;
								}
							}
						}
						if (!esta) {
							modelEspeciesUsar.addElement((String)comando);
						}
					}
				}
			}
		});
		
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Object[] comandos = modelEspecies.toArray();
				Object[] comandosSel = modelEspeciesUsar.toArray();
				if (comandos != null && comandos.length > 0) {
					for (Object comando : comandos) {
						boolean esta = false;
						if (comandosSel != null && comandosSel.length > 0) {
							for (Object comandoSel : comandosSel) {
								if (comando.equals(comandoSel)) {
									esta = true;
									break;
								}
							}
						}
						if (!esta) {
							modelEspeciesUsar.addElement((String)comando);
						}
					}
				}
			}
		});
		
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				modelEspeciesUsar.clear();
			}
		});
		
				button_3.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						Object[] comandos = (Object[])listEspeciesUsar.getSelectedValues();
						if (comandos != null && comandos.length > 0) {
							for (Object comando : comandos) {
								modelEspeciesUsar.removeElement(comando);
							}
						}
					}
				});
				
				textFieldSemilla.setText(Integer.toString((int)(Math.random()*1000000)));
				
				JLabel lblNmeroBrowsers = new JLabel("Número browsers");
				lblNmeroBrowsers.setBounds(35, 324, 130, 16);
				panel_6.add(lblNmeroBrowsers);
				
				txtNumBrowser = new JTextField();
				txtNumBrowser.setText("1");
				txtNumBrowser.setBounds(162, 321, 61, 22);
				panel_6.add(txtNumBrowser);
				txtNumBrowser.setColumns(10);
				
				JButton btnVerReporte = new JButton("Ver Reporte");
				btnVerReporte.setBounds(429, 351, 124, 25);
				panel_6.add(btnVerReporte);
				
				
				btnEjecutar2.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						if (camposObligatoriosWeb()) {
							ArchivosUtil.excluir(DIRECTORIO_GREMLIN+"reporte/reporte.html", "tbody" , "tbody");
							String[] distribucion = textFieldDitribucion2.getText().split(",");
							Object[] objEspecies = modelEspeciesUsar.toArray();
							float suma = 0;
							for (String d : distribucion) {
								suma += Float.valueOf(d.trim());
							}
							if (suma == 1.0) {
								if (distribucion.length == objEspecies.length) {
									List<String> lEspecies = new ArrayList<>();
									for (Object o : objEspecies) {
										lEspecies.add(o.toString());
									}
									
									WebTestService webTestService = new WebTestService();
									webTestService.ejecutarGremlin(txtHttp.getText(), 
																   chckbxHeadless.isSelected(), 
																   Integer.valueOf(textFieldSemilla.getText()),
																   lEspecies.toArray(new String[] {}), 
																   textFieldDitribucion2.getText(),
																   Integer.valueOf(txtNumBrowser.getText()));
									btnEjecutar2.setEnabled(false);
									boolean band = true;
									while (band) {
										int cont = 0;
										Set<Thread> threadSet = Thread.getAllStackTraces().keySet();
										for (Thread t : threadSet) {
											if (t.getName().contains("Thread-TestService")) {
												cont++;
											}
										}
										if (cont == 0) {
											band=false;
											btnEjecutar2.setEnabled(true);									
											textAreaConsola2.setText(WebTestService.strLog.toString());
											ArchivosUtil.cambiarTexto(DIRECTORIO_GREMLIN+"reporte/reporte.html", "/thead", "</thead>\n<tbody>\n"+WebTestService.strLogReporteHTML.toString());
										}
									}
								}else {
									JOptionPane.showMessageDialog(null, "ERROR La distribucion debe ser igual al numero de comandos a usar");
								}
							}
							else {
								JOptionPane.showMessageDialog(null, "ERROR La suma de las distribuciones debe ser igual 1 ");
							}
						}else {
							JOptionPane.showMessageDialog(null, "ERROR campos obligatorios");
						}
					}
				});
		
		// Actions
		rdbtnAndroid.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel_5.setVisible(false);
				panel_4.setVisible(true);
				panel_7.setVisible(false);
			}
		});
		
		rdbtnWeb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel_4.setVisible(false);
				panel_5.setVisible(true);
				panel_7.setVisible(false);
			}
		});
		
		rdbtnMutante.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panel_4.setVisible(false);
				panel_5.setVisible(false);
				panel_7.setVisible(true);
			}
		});
		
		
		button_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Object[] comandos = (Object[])list.getSelectedValues();
				Object[] comandosSel = modelMutantesUsar.toArray();
				if (comandos != null && comandos.length > 0) {
					for (Object comando : comandos) {
						boolean esta = false;
						if (comandosSel != null && comandosSel.length > 0) {
							for (Object comandoSel : comandosSel) {
								if (comando.equals(comandoSel)) {
									esta = true;
									break;
								}
							}
						}
						if (!esta) {
							modelMutantesUsar.addElement((String)comando);
						}
					}
				}
			}
		});
		
		button_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Object[] comandos = modelMutantes.toArray();
				Object[] comandosSel = modelMutantesUsar.toArray();
				if (comandos != null && comandos.length > 0) {
					for (Object comando : comandos) {
						boolean esta = false;
						if (comandosSel != null && comandosSel.length > 0) {
							for (Object comandoSel : comandosSel) {
								if (comando.equals(comandoSel)) {
									esta = true;
									break;
								}
							}
						}
						if (!esta) {
							modelMutantesUsar.addElement((String)comando);
						}
					}
				}
			}
		});
		
		button_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				modelMutantesUsar.clear();
			}
		});
		
		button_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Object[] comandos = (Object[])list_1.getSelectedValues();
				if (comandos != null && comandos.length > 0) {
					for (Object comando : comandos) {
						modelMutantesUsar.removeElement(comando);
					}
				}
			}
		});
		
		btnCrearMutantes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (camposObligatoriosMutantes()) {
					MutanteService t = new MutanteService();
					Object[] objMutantes = modelMutantesUsar.toArray();
					if (objMutantes != null && objMutantes.length > 0) {
						List<String> lMutantes = new ArrayList<>();
						for (Object o : objMutantes) {
							lMutantes.add(o.toString());
						}
						t.crearMutantes(txtC.getText(), lMutantes.toArray(new String[] {}), Integer.valueOf(textField_3.getText()));
						StringBuilder strArchivosNull = new StringBuilder();
						for (int i=0;i<t.getClasesNull().size();i++) {
							strArchivosNull.append("<tr>\n");
							strArchivosNull.append("<th scope=\"row\">"+(i+1)+"</th>\n");
							strArchivosNull.append("<td>"+t.getClasesNull().get(i)+"</td>\n");
							strArchivosNull.append("</tr>\n");
						}
						StringBuilder strArchivosAOR = new StringBuilder();
						for (int i=0;i<t.getClasesAOR().size();i++) {
							strArchivosAOR.append("<tr>\n");
							strArchivosAOR.append("<th scope=\"row\">"+(i+1)+"</th>\n");
							strArchivosAOR.append("<td>"+t.getClasesAOR().get(i)+"</td>\n");
							strArchivosAOR.append("</tr>\n");
						}
						StringBuilder strArchivosCOR = new StringBuilder();
						for (int i=0;i<t.getClasesCOR().size();i++) {
							strArchivosCOR.append("<tr>\n");
							strArchivosCOR.append("<th scope=\"row\">"+(i+1)+"</th>\n");
							strArchivosCOR.append("<td>"+t.getClasesCOR().get(i)+"</td>\n");
							strArchivosCOR.append("</tr>\n");
						}
						ArchivosUtil.excluir(DIRECTORIO_GREMLIN+"reporte/reporteMutante.html", "tbody class=\"t1\"" , "tbody");
						ArchivosUtil.excluir(DIRECTORIO_GREMLIN+"reporte/reporteMutante.html", "tbody class=\"t2\"" , "tbody");
						ArchivosUtil.excluir(DIRECTORIO_GREMLIN+"reporte/reporteMutante.html", "tbody class=\"t3\"" , "tbody");
						ArchivosUtil.cambiarTexto(DIRECTORIO_GREMLIN+"reporte/reporteMutante.html", "class=\"t1\"", "<tbody class=\"t1\">\n"+strArchivosAOR.toString());
						ArchivosUtil.cambiarTexto(DIRECTORIO_GREMLIN+"reporte/reporteMutante.html", "class=\"t2\"", "<tbody class=\"t2\">\n"+strArchivosCOR.toString());
						ArchivosUtil.cambiarTexto(DIRECTORIO_GREMLIN+"reporte/reporteMutante.html", "class=\"t3\"", "<tbody class=\"t3\">\n"+strArchivosNull.toString());
						JOptionPane.showMessageDialog(null, "Mutantes creados");
					}else {
						JOptionPane.showMessageDialog(null, "ERROR campos obligatorios");
					}
				}else {
					JOptionPane.showMessageDialog(null, "ERROR campos obligatorios");
				}
			}
		});
				
		btnVerReporte.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String filePathString = DIRECTORIO_GREMLIN + "reporte/reporte.html";
				
				File f = new File(filePathString);
				if(!f.exists()) { 
					JOptionPane.showMessageDialog(null, "No existe reporte generado.");
				}else {
					URL url = null;
					try {
						url = new URL("file:///" + filePathString);
						openWebpage(url);
					} catch (MalformedURLException exp) {
						// TODO Auto-generated catch block
						JOptionPane.showMessageDialog(null, "ERROR "+exp.getMessage());
					}
				}
			}
		});
		
		btnReporteMutantes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String filePathString = DIRECTORIO_GREMLIN + "reporte/reporteMutante.html";
				
				File f = new File(filePathString);
				if(!f.exists()) { 
					JOptionPane.showMessageDialog(null, "No existe reporte generado.");
				}else {
					URL url = null;
					try {
						url = new URL("file:///" + filePathString);
						openWebpage(url);
					} catch (MalformedURLException exp) {
						// TODO Auto-generated catch block
						JOptionPane.showMessageDialog(null, "ERROR "+exp.getMessage());
					}
				}
			}
		});
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			JButton b = (JButton)e.getSource();
			if (b.getText().equals(">")) {
				Object[] comandos = (Object[])this.listComandos.getSelectedValues();
				Object[] comandosSel = this.modelComandosUsar.toArray();
				if (comandos != null && comandos.length > 0) {
					for (Object comando : comandos) {
						boolean esta = false;
						if (comandosSel != null && comandosSel.length > 0) {
							for (Object comandoSel : comandosSel) {
								if (comando.equals(comandoSel)) {
									esta = true;
									break;
								}
							}
						}
						if (!esta) {
							this.modelComandosUsar.addElement((String)comando);
						}
					}
				}
			}else if (b.getText().equals(">>")) {
				Object[] comandos = this.modelComandos.toArray();
				Object[] comandosSel = this.modelComandosUsar.toArray();
				if (comandos != null && comandos.length > 0) {
					for (Object comando : comandos) {
						boolean esta = false;
						if (comandosSel != null && comandosSel.length > 0) {
							for (Object comandoSel : comandosSel) {
								if (comando.equals(comandoSel)) {
									esta = true;
									break;
								}
							}
						}
						if (!esta) {
							this.modelComandosUsar.addElement((String)comando);
						}
					}
				}
			}else if (b.getText().equals("<<")) {
				modelComandosUsar.clear();
			}else if (b.getText().equals("<")) {
				Object[] comandos = (Object[])this.listComandosUsar.getSelectedValues();
				if (comandos != null && comandos.length > 0) {
					for (Object comando : comandos) {
						this.modelComandosUsar.removeElement(comando);
					}
				}
			}else if(b.getText().equalsIgnoreCase("ejecutar")) {
				if (camposObligatorios()) {
					ArchivosUtil.excluir(DIRECTORIO_GREMLIN+"reporte/reporte.html", "tbody", "tbody");
					String[] distribucion = textFieldDistribucion.getText().split(",");
					Object[] comandos = (Object[])this.modelComandosUsar.toArray();
					float suma = 0;
					for (String d : distribucion) {
						suma += Float.valueOf(d.trim());
					}
					if (suma == 1.0) {
						if (distribucion.length == comandos.length) {
							if (primeraVez) {
								
								iniciar();
								primeraVez = false;
							}
							ejecutar();
						}else {
							JOptionPane.showMessageDialog(null, "ERROR La distribucion debe ser igual al numero de comandos a usar");
						}
					}
					else {
						JOptionPane.showMessageDialog(null, "ERROR La suma de las distribuciones debe ser igual 1 ");
					}
				}else {
					JOptionPane.showMessageDialog(null, "ERROR campos obligatorios");
				}
			}
		}		
	}

	private boolean camposObligatoriosWeb() {
		boolean obligatorio = true;
		if (txtHttp.getText() == null || txtHttp.getText().isEmpty() ||
				textFieldDitribucion2.getText() == null || textFieldDitribucion2.getText().isEmpty() || 
				modelEspeciesUsar== null || modelEspeciesUsar.isEmpty() ||
				textFieldSemilla.getText() == null || textFieldSemilla.getText().isEmpty() ||
				txtNumBrowser.getText() == null || txtNumBrowser.getText().isEmpty()) {
			obligatorio = false;
		}
		return obligatorio;
	}

	private boolean camposObligatoriosMutantes() {
		boolean obligatorio = true;
		if (txtC.getText() == null || txtC.getText().isEmpty() ||
				textField_3.getText() == null || textField_3.getText().isEmpty()) {
			obligatorio = false;
		}
		return obligatorio;
	}
	
	private boolean camposObligatorios() {
		boolean obligatorio = true;
		if (	textFieldNroEventos.getText() == null || textFieldNroEventos.getText().isEmpty() || 
				textFieldPaquete.getText() == null || textFieldPaquete.getText().isEmpty() ||
				textFieldRutaApk.getText() == null || textFieldRutaApk.getText().isEmpty() ||
				txtRutaADB.getText() == null || txtRutaADB.getText().isEmpty() ||
				textFieldDistribucion.getText() == null || textFieldDistribucion.getText().isEmpty()) {
			obligatorio = false;
		}
		return obligatorio;
	}
	
	private void iniciar() {
		try {
			Process adbUninstall = rt.exec(txtRutaADB.getText()+Constantes.ADB_UNINSTALL_APK+textFieldPaquete.getText());
			adbUninstall.waitFor();
			Process adbInstall = rt.exec(txtRutaADB.getText()+Constantes.ADB_INSTALL_APK+textFieldRutaApk.getText());
			adbInstall.waitFor();
			Process adbStart = rt.exec(txtRutaADB.getText()+Constantes.ADB_START_APP_1+textFieldPaquete.getText()+Constantes.ADB_START_APP_2);
			adbStart.waitFor();
		}catch(Exception exc) {
			JOptionPane.showMessageDialog(null, "ERROR "+exc.getMessage());
		}
	}
	
	private void ejecutar() {
		
		try {
			int numeroEventos = Integer.valueOf(textFieldNroEventos.getText()); 
	        Random random = new Random(12345);
	        mapaDistribucion = new HashMap<>();
	        Constantes.generarDistribucion(mapaDistribucion, this.modelComandosUsar.toArray(), this.textFieldDistribucion.getText().split(","), numeroEventos);
	        System.out.println(mapaDistribucion);
	        textAreaConsola.setText(textAreaConsola.getText()+mapaDistribucion+"\n");
	        StringBuilder strLogReporteHTML = new StringBuilder();
			for (int i=0;i<numeroEventos;i++) {
				Object comando = Constantes.obtenerComandoRandom(this.modelComandosUsar.toArray(),mapaDistribucion);
				String argumentos = "";
				String sentencia = Constantes.COMANDOS.get(comando);
				if (comando.toString().equals("tap")) {
					int x = random.nextInt(Constantes.ANCHO);
	                int y = random.nextInt(Constantes.ALTO);
	                argumentos = x + " " + y;
				}else if (comando.toString().equals("swipe")) {
					int x1 = random.nextInt(Constantes.ANCHO);
	                int y1 = random.nextInt(Constantes.ALTO);
	                int x2 = random.nextInt(Constantes.ANCHO);
	                int y2 = random.nextInt(Constantes.ALTO);
	                argumentos = x1 + " " + y1 + " " + x2 + " " + y2;
				}else if (comando.toString().equals("text")) {
					argumentos = Constantes.getPalabra(6);
				}else if (comando.toString().equals("keyevent")) {
					argumentos = Constantes.getKeyevent() + "";
				}
				if (sentencia.contains("adb")) {
					rt.exec(txtRutaADB.getText()+sentencia+argumentos);
				}
				System.out.println("sentencia "+sentencia);
				textAreaConsola.setText(textAreaConsola.getText()+("sentencia "+sentencia)+"\n");
				strLogReporteHTML.append("<tr>\n");
				strLogReporteHTML.append("<th scope=\"row\">"+(i+1)+"</th>\n");
				strLogReporteHTML.append("<td>"+"sentencia "+sentencia+"</td>\n");
				strLogReporteHTML.append("</tr>\n");
				Thread.sleep(3000);
			}
			ArchivosUtil.cambiarTexto(DIRECTORIO_GREMLIN+"reporte/reporte.html", "/thead", "</thead>\n<tbody>\n"+strLogReporteHTML.toString());
	        System.out.println(mapaDistribucion);
			textAreaConsola.setText(textAreaConsola.getText()+mapaDistribucion+"\n");
		}catch(Exception e) {
			JOptionPane.showMessageDialog(null, "ERROR "+e.getMessage());
		}
	}
	
	public static void openWebpage(URI uri) {
	    Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
	    if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
	        try {
	            desktop.browse(uri);
	        } catch (Exception e) {
	        	JOptionPane.showMessageDialog(null, "ERROR "+e.getMessage());
	        }
	    }
	}

	public static void openWebpage(URL url) {
	    try {
	        openWebpage(url.toURI());
	    } catch (URISyntaxException e) {
	    	JOptionPane.showMessageDialog(null, "ERROR "+e.getMessage());
	    }
	}
}