package co.edu.uniandes.testtool.logica;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import co.edu.uniandes.testtool.util.ArchivosUtil;
import co.edu.uniandes.testtool.util.Constantes;

public class WebTestService implements Runnable{

	public static String DIRECTORIO_GREMLIN = "";
	public static final String ARCHIVO_CAMBIO_URL = "wdio.conf.js";
	public static final String ARCHIVO_GREMLIN = "test/specs/gremlins-test.js";
	private ProcessBuilder builder;
	public static StringBuilder strLog = new StringBuilder();
	public static StringBuilder strLogReporteHTML = new StringBuilder();

	public void ejecutarGremlin(String url, boolean esHeadless, Integer semilla, String[] especies, String distribucion, Integer numBrowser) {
		strLog = new StringBuilder();
		strLogReporteHTML = new StringBuilder();
		String headless = "{}";
		if (esHeadless) {
			headless = "{args: ['headless', 'disable-gpu'],}";
		}					

		//Cambiar settings wdio.conf.js url
		ArchivosUtil.cambiarTexto(DIRECTORIO_GREMLIN+ARCHIVO_CAMBIO_URL, "baseUrl: ", "    baseUrl: '"+url+"',");
		//Cambiar settings wdio.conf.js headless
		ArchivosUtil.cambiarTexto(DIRECTORIO_GREMLIN+ARCHIVO_CAMBIO_URL, "chromeOptions: ", "        chromeOptions: "+headless);
		//Semilla
		//Cambiar test gremlin semilla
		ArchivosUtil.cambiarTexto(DIRECTORIO_GREMLIN+ARCHIVO_GREMLIN, "horde.seed", "    horde.seed("+semilla+");");
		StringBuilder str = new StringBuilder();
		for (String especie : especies) {
			//Adicionar especies
			str.append(Constantes.ESPECIES.get(especie)).append("\n");
		}
		ArchivosUtil.cambiarTexto(DIRECTORIO_GREMLIN+ARCHIVO_GREMLIN, "horde.gremlin(gremlins.species", "");
		ArchivosUtil.cambiarTexto(DIRECTORIO_GREMLIN+ARCHIVO_GREMLIN, "//species", "        //species \n"+str.toString());
		ArchivosUtil.cambiarTexto(DIRECTORIO_GREMLIN+ARCHIVO_GREMLIN, "var distributionStrategy", "    var distributionStrategy = gremlins.strategies.distribution(["+distribucion+"]);");
		//Browsers
		for (int i=1;i<=numBrowser;i++) {
			Thread hilo = new Thread(this,"Thread-TestService"+i);
			hilo.start();
		}
	}

	public void run() {
		BufferedReader r = null;
		Process p = null;
		try {
			builder = new ProcessBuilder(
					"cmd.exe", 
					"/c", 
					"cd " +DIRECTORIO_GREMLIN + " && npm test");
			builder.redirectErrorStream(true);
			p = builder.start();
			r = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String line;
			int cont = 1;
			while (true) {
				line = r.readLine();
				if (line == null) { break; }
				strLog.append(line+"\n");
				strLogReporteHTML.append("<tr>\n");
				strLogReporteHTML.append("<th scope=\"row\">"+(cont++)+"</th>\n");
				strLogReporteHTML.append("<td>"+line+"</td>\n");
				strLogReporteHTML.append("</tr>\n");
				System.out.println(line);
			}
			Thread.sleep(5000);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				r.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			p.destroy();
		}
	}
}
