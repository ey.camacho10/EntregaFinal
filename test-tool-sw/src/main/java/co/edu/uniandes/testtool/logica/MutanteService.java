package co.edu.uniandes.testtool.logica;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

import co.edu.uniandes.testtool.util.ArchivosUtil;

public class MutanteService {

	private List<File> archivos;
	private List<Integer> indicesArchivosUtilizados;
	private Integer cantidadNull;
	private Integer cantidadCOR;
	private Integer cantidadAOR;
	private List<String> clasesAOR;
	private List<String> clasesCOR;
	private List<String> clasesNull;
	private List<Integer> indicesUtilizados;


	public void crearMutantes(String strDirectorio, String[] tiposMutantes, Integer numeroMutantes) {
		File directorio = new File(strDirectorio);
		if (directorio.isDirectory()) {
			File dirMutante = new File(strDirectorio+"/mutantes");
			if (dirMutante.exists()) {
				Path pathDirMutante= Paths.get(dirMutante.getPath());
				try {
					Files.walk(pathDirMutante, FileVisitOption.FOLLOW_LINKS)
					.sorted(Comparator.reverseOrder())
					.map(Path::toFile)
					.peek(System.out::println)
					.forEach(File::delete);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			cantidadAOR = cantidadCOR = cantidadNull = 0;
			archivos = new ArrayList<>();
			indicesArchivosUtilizados = new ArrayList<>();
			indicesUtilizados = new ArrayList<>();
			clasesAOR = new ArrayList<>();
			clasesCOR = new ArrayList<>();
			clasesNull = new ArrayList<>();
			buscarArchivos(directorio);
			for (int j=0;j<tiposMutantes.length;j++) {
				for (int i=0;i<numeroMutantes;i++) {
					Integer indexArchivo = buscarRandomArchivo(archivos.size());
					if (indexArchivo != null) {
						File archivoSeleccionado = archivos.get(indexArchivo);
						Path origen = Paths.get(archivoSeleccionado.getPath());
						String[] split = archivoSeleccionado.getPath().split(directorio.getName());
						Path destino = Paths.get(directorio.getPath()+"/mutantes/"+split[split.length-1]);
						try {
							Files.createDirectories(destino.getParent());
							Files.copy(origen,destino, StandardCopyOption.REPLACE_EXISTING);
							//Crear mutantes
							switch (tiposMutantes[j]) {
							case "NullIntent":
								boolean encontro1= false;
								int cont1 = 1;
								do {
									List<String> lineasOcurrencias = ArchivosUtil.ocurrencias(archivoSeleccionado, "= new ");
									if (!lineasOcurrencias.isEmpty()) {
										Random random = new Random();
										int indexOcurrencia = random.nextInt(lineasOcurrencias.size());
										String lineaRandom = lineasOcurrencias.get(indexOcurrencia);
										String[] lineaSplit = lineaRandom.split("= new ");
										String mutante = lineaSplit[0]+"= null;";
										ArchivosUtil.cambiarTextoMutante(destino.toString(), lineaRandom, mutante);
										encontro1 = true;
										indicesUtilizados.add(indexArchivo);
										System.out.println("NullIntent: " +destino.toString());
										clasesNull.add(destino.toString());
										cantidadNull++;
									}else {
										try {
											Files.delete(destino);
										}catch(Exception e) {}
									}
									if (!encontro1) {
										indexArchivo = buscarRandomArchivo(archivos.size());
										if (indexArchivo != null) {
											archivoSeleccionado = archivos.get(indexArchivo);
											origen = Paths.get(archivoSeleccionado.getPath());
											split = archivoSeleccionado.getPath().split(directorio.getName());
											destino = Paths.get(directorio.getPath()+"/mutantes/"+split[split.length-1]);
											if (cont1 + 1 < 11) {
												Files.createDirectories(destino.getParent());
												Files.copy(origen,destino, StandardCopyOption.REPLACE_EXISTING);
											}
										}
									}
									cont1++;
								}while(!encontro1 && cont1<11);
								break;
							case "COR":
								boolean encontro2= false;
								int cont2 = 1;
								do {
									List<String> lOcurrenciasCORSimples = ArchivosUtil.ocurrenciasCORSimples(archivoSeleccionado);
									if (!lOcurrenciasCORSimples.isEmpty()) {
										Random random = new Random();
										int indexOcurrencia = random.nextInt(lOcurrenciasCORSimples.size());
										String lineaRandom = lOcurrenciasCORSimples.get(indexOcurrencia);
										String mutante = "";
										if (lineaRandom.contains("&&")) {
											mutante = lineaRandom.replaceAll("&&", "||");
										}else if (lineaRandom.contains("||")) {
											mutante = lineaRandom.replaceAll("\\|\\|", "&&");
										}
										ArchivosUtil.cambiarTextoMutante(destino.toString(), lineaRandom, mutante);
										encontro2 = true;
										indicesUtilizados.add(indexArchivo);
										System.out.println("COR: " +destino.toString());
										clasesCOR.add(destino.toString());
										cantidadCOR++;
									}else {
										try {
											Files.delete(destino);
										}catch(Exception e) {}
									}
									if (!encontro2) {
										indexArchivo = buscarRandomArchivo(archivos.size());
										if (indexArchivo != null) {
											archivoSeleccionado = archivos.get(indexArchivo);
											origen = Paths.get(archivoSeleccionado.getPath());
											split = archivoSeleccionado.getPath().split(directorio.getName());
											destino = Paths.get(directorio.getPath()+"/mutantes/"+split[split.length-1]);
											if (cont2 + 1 < 11) {
												Files.createDirectories(destino.getParent());
												Files.copy(origen,destino, StandardCopyOption.REPLACE_EXISTING);
											}
										}
									}
									cont2++;
								}while(!encontro2 && cont2<11);
								break;
							case "AOR Increment":
								boolean encontro3= false;
								int cont3 = 1;
								do {
									List<String> lOcurrenciasAORIncrement = ArchivosUtil.ocurrenciasAORIncrement(archivoSeleccionado);
									if (!lOcurrenciasAORIncrement.isEmpty()) {
										Random random = new Random();
										int indexOcurrencia = random.nextInt(lOcurrenciasAORIncrement.size());
										String lineaRandom = lOcurrenciasAORIncrement.get(indexOcurrencia);
										String mutante = lineaRandom.replaceAll("\\+\\+", "--");

										ArchivosUtil.cambiarTextoMutante(destino.toString(), lineaRandom, mutante);
										encontro3 = true;
										indicesUtilizados.add(indexArchivo);
										clasesAOR.add(destino.toString());
										System.out.println("AOR Increment: " +destino.toString());
										cantidadAOR++;
									}else {
										try {
											Files.delete(destino);
										}catch(Exception e) {}
									}
									if (!encontro3) {
										indexArchivo = buscarRandomArchivo(archivos.size());
										if (indexArchivo != null) {
											archivoSeleccionado = archivos.get(indexArchivo);
											origen = Paths.get(archivoSeleccionado.getPath());
											split = archivoSeleccionado.getPath().split(directorio.getName());
											destino = Paths.get(directorio.getPath()+"/mutantes/"+split[split.length-1]);
											if (cont3 + 1 < 16) {
												Files.createDirectories(destino.getParent());
												Files.copy(origen,destino, StandardCopyOption.REPLACE_EXISTING);
											}
										}
									}
									cont3++;
								}while(!encontro3 && cont3<16);
								break;
							}
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
				indicesArchivosUtilizados = new ArrayList<>(indicesUtilizados); 
			}
			System.out.println("CantidadAOR "+clasesAOR.size());
			System.out.println("CantidadNull "+clasesNull.size());
			System.out.println("CantidadCOR "+clasesCOR.size());
			ArchivosUtil.cambiarTexto(WebTestService.DIRECTORIO_GREMLIN+"reporte/reporteMutante.html", "arg_1", "<td class=\"arg_1\">"+cantidadAOR+"</td>");
			ArchivosUtil.cambiarTexto(WebTestService.DIRECTORIO_GREMLIN+"reporte/reporteMutante.html", "arg_2", "<td class=\"arg_2\">"+cantidadCOR+"</td>");
			ArchivosUtil.cambiarTexto(WebTestService.DIRECTORIO_GREMLIN+"reporte/reporteMutante.html", "arg_3", "<td class=\"arg_3\">"+cantidadNull+"</td>");
			ArchivosUtil.cambiarTexto(WebTestService.DIRECTORIO_GREMLIN+"reporte/reporteMutante.html", "arg_4", "<td class=\"arg_4\">"+(cantidadNull+cantidadAOR+cantidadCOR)+"</td>");
			ArchivosUtil.cambiarTexto(WebTestService.DIRECTORIO_GREMLIN+"reporte/reporteMutante.html", "arg_5", "<td class=\"arg_5\">"+(numeroMutantes*tiposMutantes.length)+"</td>");
			double cantidadGenerados = cantidadNull+cantidadAOR+cantidadCOR;
			double cantidadEsperados = numeroMutantes*tiposMutantes.length;
			double res = cantidadGenerados / cantidadEsperados;
			ArchivosUtil.cambiarTexto(WebTestService.DIRECTORIO_GREMLIN+"reporte/reporteMutante.html", "arg_6", "<td class=\"arg_6\">"+(res*100)+" % </td>");
		}
	}

	private void buscarArchivos(File directorio) {
		if (directorio.isFile()) {
			archivos.add(directorio);
		}else if (directorio.isDirectory()) {
			archivos.addAll(Arrays.asList(directorio.listFiles(new FileFilter() {
				@Override
				public boolean accept(File pathname) {
					String name = pathname.getName().toLowerCase();
					return name.endsWith(".java") && pathname.isFile() && !pathname.getPath().toLowerCase().contains("test");
				}
			})
					));
			List<File> directorios = Arrays.asList(
					directorio.listFiles(new FileFilter() {
						@Override
						public boolean accept(File pathname) {
							return pathname.isDirectory() && !pathname.getPath().toLowerCase().contains("test");
						}
					}
							));
			if (directorios != null && !directorios.isEmpty()) {
				for(File dir : directorios) {
					buscarArchivos(dir);
				}
			}
		}

	}

	private Integer buscarRandomArchivo(Integer tamanioArchivos) {
		Random randomArchivo = new Random();
		Integer res = null;
		Integer index = null;
		boolean band = false;
		Integer cant = 0;

		while(!band && cant < tamanioArchivos) {
			index = randomArchivo.nextInt(tamanioArchivos);
			boolean bandInt = false;
			for (Integer indiceUtilizado : indicesArchivosUtilizados) {
				if (indiceUtilizado.intValue() == index.intValue()) {
					bandInt = true;
					break;
				}
			}
			if (!bandInt) {
				indicesArchivosUtilizados.add(index);
				res = index;
				band=true;
			}
			cant++;
		}

		return res;
	}

	public Integer getCantidadNull() {
		return cantidadNull;
	}

	public void setCantidadNull(Integer cantidadNull) {
		this.cantidadNull = cantidadNull;
	}

	public Integer getCantidadCOR() {
		return cantidadCOR;
	}

	public void setCantidadCOR(Integer cantidadCOR) {
		this.cantidadCOR = cantidadCOR;
	}

	public Integer getCantidadAOR() {
		return cantidadAOR;
	}

	public void setCantidadAOR(Integer cantidadAOR) {
		this.cantidadAOR = cantidadAOR;
	}

	public List<String> getClasesAOR() {
		return clasesAOR;
	}

	public void setClasesAOR(List<String> clasesAOR) {
		this.clasesAOR = clasesAOR;
	}

	public List<String> getClasesCOR() {
		return clasesCOR;
	}

	public void setClasesCOR(List<String> clasesCOR) {
		this.clasesCOR = clasesCOR;
	}

	public List<String> getClasesNull() {
		return clasesNull;
	}

	public void setClasesNull(List<String> clasesNull) {
		this.clasesNull = clasesNull;
	}
}
